# Juniper Structured Syslog
----

This pack replicates and expands on the Splunk Add-on for Juniper.
Currently it parses Juniper Structured Data format logs for the following sources:
* RT_FLOW
* SECINTEL
* RT_AAMW
* SNMP_TRAP_LINK_UP
* SNMP_TRAP_LINK_DOWN
* UI_LOGIN_EVENT
* UI_LOGOUT_EVENT
* UI_CMDLINE_READ_LINE

### Note:
This pack does work when using the brief flag along with the structured data flag but does **not** currently support netscreen logs or non structured data formated logs


## Requirements Section

Before you begin, ensure that you have met the following requirements:
* Ensure that the Juniper Devices intended to send logs are able to reach the Cribl Stream workers over the required port/protocol
* Juniper devices using a `system syslog` config with the `structured-data` flag

Example:
```                                     
host <cribl stream ip or dns name>  {
    any any;
    user info;
    change-log info;
    interactive-commands info;
    structured-data;
}
```
* Install the pack
  * Create a syslog source or using an existing syslog source
  * Create a route configure it to use this pack and the desired destination
* The Splunk Add-on for Juniper 
  * This is mainly being used for the eventtypes
* Create event types for the following
### Login and Logout events
```
[juniper_junos_authentication_network]
search = sourcetype=juniper:junos:login OR sourcetype=juniper:junos:logout
#tags = authentication network
```
### Command Line auditing
```
[juniper_junos_commandline_change_network]
search = sourcetype=juniper:junos:commandline
#tags = change network
```


## Release Notes
### Version 1.0.2 - 2023-10-01
Make host regex more generalized

### Version 1.0.1 - 2023-09-05
Resolve ID issue with Pack

### Version 1.0.0 - 2023-08-05
The inital release
* Includes parsing for:
  * Structured Firewall
  * Structured AAMW
  * Structured SECINTEL
  * Structured SNMP
  * Structured Login
  * Structured Logout
  * Structured Commandline
  * JSON and Key Value support

## Contributing to the Pack
To contribute to the Pack, please do the following:
Go to the repo [here](https://gitlab.com/wkleinhenz/cc-juniper-structured-syslog)

If requesting a new message type please include an anonomyzed sample of the desired message type

Issues and Merge Reqests are welcome and appreciated



## Contact
To contact us please email 13kleiw@gmail.com.


## License
This Pack uses the following license: [`Apache 2.0`](https://www.apache.org/licenses/LICENSE-2.0.txt).
